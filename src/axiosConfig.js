import axios from 'axios';
const instance = axios.create({
  baseURL: 'https://hoodwink.medkomtek.net/api'
});

export default instance;
