export default {
  // REGISTER -------------------------------------
  register: `/register`,

  // LOGIN ----------------------------------------
  login: `/auth/login`,

  // ADD PRODUCT ----------------------------------
  addProduct: `/item/add`,

  // UPDATE PRODUCT -------------------------------
  updateProduct: `/item/update`,

  // DELETE PRODUCT -------------------------------
  deleteProduct: `/item/delete`,

  // GET LIST ITEMS -------------------------------
  getListItems: `/items`,

  // GET PRODUCT LIST BY SKU ----------------------
  getItem: `/item/search`
};
