import axios from '../axiosConfig';
import endPoint from './endpoint';

export const getListItems = async ({
  setListProduct,
  searchInputValue,
  setNotExist
}) => {
  const token = localStorage.getItem('token');
  const header = {
    Authorization: 'Bearer',
    token
  };
  let list = [];
  if (searchInputValue) {
    const sku = { sku: searchInputValue };
    const data = await axios.post(endPoint.getItem, sku, header);
    if (data.data.data !== null) {
      list = [data.data];
      setListProduct(list);
      setNotExist(false);
    } else {
      setNotExist(true);
    }
  } else {
    const data = await axios.get(endPoint.getListItems, header);
    if (data) {
      setListProduct(data.data);
    }
  }
};
