const autoCompleteInitialItem = [
  {
    id: 1,
    url: '/pages/events',
    breadCrumbTexts: ['Pages ', ' Events'],
    catagories: 'recentlyBrowsedItems'
  },
  {
    id: 2,
    url: '/e-commerce/customers',
    breadCrumbTexts: ['E-commerce ', ' Customers'],
    catagories: 'recentlyBrowsedItems'
  }
];

export default autoCompleteInitialItem;
