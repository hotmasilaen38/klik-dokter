import React from 'react';
import PropTypes from 'prop-types';
import Background from 'components/common/Background';
import { Card, Col, Row, Container } from 'react-bootstrap';

const AuthSplitLayout = ({ children }) => {
  return (
    <Container fluid>
      <Col sm={10} md={6} className="px-sm-0 align-self-center mx-auto py-5">
        <Row className="g-0 justify-content-center">
          <Col lg={9} xl={8} className="col-xxl-6">
            <Card>
              <Card.Body className="px-0 py-4">{children}</Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    </Container>
  );
};

AuthSplitLayout.propTypes = {
  children: PropTypes.node.isRequired,
  bgProps: PropTypes.shape(Background.propTypes).isRequired
};

export default AuthSplitLayout;
