const INITIAL_STATE = {
  user: null
};

// REDUCERS
export const rootReducer = (state = INITIAL_STATE, { type, data }) => {
  switch (type) {
    case 'LIST_PRODUCT':
      return { ...state, listProduct: data };

    default:
      return state;
  }
};
