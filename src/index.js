import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';

import { rootReducer } from './store';
import App from './App';
import Main from './Main';
import 'helpers/initFA';
import axios from 'axiosConfig';
const persistedReducer = persistReducer(
  {
    key: 'root',
    storage
  },
  rootReducer
);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

const token = localStorage.getItem('token');
if (token) {
  axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
}

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Main>
        <App />
      </Main>
    </PersistGate>
  </Provider>,
  document.getElementById('main')
);
