import React from 'react';
import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

const loginButton = () => {
  return (
    <Link to="/authentication/card/login">
      <Button variant="primary" style={{ height: '50px' }}>
        LOGIN
      </Button>
    </Link>
  );
};

const logoutAction = ({ history }) => {
  localStorage.removeItem('token');
  location.reload();
};

const logoutButton = () => {
  return (
    <Button variant="danger" style={{ height: '50px' }} onClick={logoutAction}>
      LOGOUT
    </Button>
  );
};

const TopNavRightSideNavItem = () => {
  const token = localStorage.getItem('token');
  return (
    <Nav
      navbar
      className="navbar-nav-icons ms-auto flex-row align-items-center"
      as="ul"
    >
      <Link to="/authentication/card/register">
        <Button variant="primary" style={{ height: '50px' }} className="me-3">
          REGISTER
        </Button>
      </Link>
      {token ? logoutButton() : loginButton()}
    </Nav>
  );
};

export default TopNavRightSideNavItem;
