import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import Login from 'components/authentication/simple/Login';
import Registration from 'components/authentication/simple/Registration';

const AuthSimpleRoutes = () => {
  const { url } = useRouteMatch();

  return (
    <Switch>
      <Route
        path={`${url}/login`}
        exact
        render={props => <Login {...props} />}
      />
      <Route path={`${url}/register`} exact component={Registration} />

      {/*Redirect*/}
      <Redirect to="/errors/404" />
    </Switch>
  );
};

export default AuthSimpleRoutes;
