import React, { useState } from 'react';
import PropTypes from 'prop-types';
import axios from '../../axiosConfig';
import { Button, Form, Row } from 'react-bootstrap';
import endPoint from '../../helpers/endpoint';

const RegistrationForm = ({ hasLabel }) => {
  // State
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });
  const [isError, setIsError] = useState(false);
  const [formValidatioan, setFormValidatioan] = useState({
    email: '',
    password: ''
  });

  // Handler
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const { data } = await axios.post(endPoint.register, formData);
      if (data) {
        setIsError(false);
      }
    } catch (error) {
      if (error) {
        setIsError(true);
      }
    }
  };

  const handleFieldChange = e => {
    console.log(e.target.value);
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group className="mb-3">
        {hasLabel && <Form.Label>Email address</Form.Label>}
        <Form.Control
          placeholder={!hasLabel ? 'Email address' : ''}
          value={formData.email}
          name="email"
          onChange={handleFieldChange}
          type="text"
          style={{ height: '50px' }}
        />
      </Form.Group>

      <Row className="g-2 mb-4">
        <Form.Group>
          {hasLabel && <Form.Label>Password</Form.Label>}
          <Form.Control
            placeholder={!hasLabel ? 'Password' : ''}
            value={formData.password}
            name="password"
            onChange={handleFieldChange}
            type="password"
            style={{ height: '50px' }}
          />
        </Form.Group>
      </Row>
      <p style={{ color: isError ? 'red' : 'green' }}>
        {isError ? 'Request failed!' : 'Request succes!'}
      </p>
      <Form.Group>
        <Button
          className="w-100"
          style={{ height: '50px' }}
          type="submit"
          disabled={!formData.email || !formData.password}
        >
          Register
        </Button>
      </Form.Group>
    </Form>
  );
};

RegistrationForm.propTypes = {
  hasLabel: PropTypes.bool
};

export default RegistrationForm;
