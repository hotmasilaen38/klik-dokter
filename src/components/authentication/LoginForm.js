import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'react-bootstrap';
import axios from '../../axiosConfig';
import endPoint from '../../helpers/endpoint';

const LoginForm = ({ hasLabel, history }) => {
  // State
  const [formData, setFormData] = useState({
    email: '',
    password: '',
    remember: false
  });

  const [isError, setIsError] = useState(false);
  // Handler
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const { data } = await axios.post(endPoint.login, formData);
      if (data) {
        setIsError(false);
        const token = data.token;
        axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
        localStorage.setItem('token', token);
        history.push('/');
      }
    } catch (error) {
      if (error) {
        setIsError(true);
      }
    }
  };

  const handleFieldChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };
  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group className="mb-3">
        {hasLabel && <Form.Label>Email</Form.Label>}
        <Form.Control
          placeholder={!hasLabel ? 'Email' : ''}
          value={formData.email}
          name="email"
          onChange={handleFieldChange}
          type="email"
          style={{ height: '50px' }}
        />
      </Form.Group>

      <Form.Group className="mb-3">
        {hasLabel && <Form.Label>Password</Form.Label>}
        <Form.Control
          placeholder={!hasLabel ? 'Password' : ''}
          value={formData.password}
          name="password"
          onChange={handleFieldChange}
          type="password"
          style={{ height: '50px' }}
        />
      </Form.Group>

      <Form.Group>
        <Button
          type="submit"
          color="primary"
          style={{ height: '50px' }}
          className="mt-3 w-100"
          disabled={!formData.email || !formData.password}
        >
          Log in
        </Button>
      </Form.Group>
    </Form>
  );
};

LoginForm.propTypes = {
  layout: PropTypes.string,
  hasLabel: PropTypes.bool
};

LoginForm.defaultProps = {
  layout: 'simple',
  hasLabel: false
};

export default LoginForm;
