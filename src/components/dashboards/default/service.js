import axios from '../../../../src/axiosConfig';
import endPoint from '../../../helpers/endpoint';

export const handleSubmit = async ({ e, formData, setModalShow }) => {
  const token = localStorage.getItem('token');
  const header = {
    Authorization: 'Bearer',
    token
  };
  e.preventDefault();
  await axios.post(endPoint.addProduct, formData, header);
  setModalShow(false);
};

export const editProduct = async ({ e, formData, setEditModalShow }) => {
  const token = localStorage.getItem('token');
  const header = {
    Authorization: 'Bearer',
    token
  };
  e.preventDefault();
  await axios.post(endPoint.updateProduct, formData, header);
  setEditModalShow(false);
};

export const editForm = async ({ e, formData }) => {
  console.log(formData);
  e.preventDefault();
  //   await axios.post(endPoint.updateProduct, formData);
};
