import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import ActionButton from 'components/common/ActionButton';
import Flex from 'components/common/Flex';
import AuthSplitLayout from 'layouts/AuthSplitLayout';
import AddForm from './AddForm';
import EditForm from './EditForm';
import { handleSubmit, editProduct } from './service';
import { getListItems } from '../../../helpers/index';
import axios from '../../../axiosConfig';
import endPoint from '../../../helpers/endpoint';

const Dashboard = ({ history }) => {
  const isLogin = localStorage.getItem('token');
  const [searchInputValue, setSearchInputValue] = useState('');
  const [listProduct, setListProduct] = useState([]);
  const [notExist, setNotExist] = useState(false);
  const [modalShow, setModalShow] = React.useState(false);
  const [editModalShow, setEditModalShow] = React.useState(false);
  const [selectedId, setSelectedId] = useState('');
  const [formData, setFormData] = useState({
    sku: '',
    product_name: '',
    qty: '',
    price: '',
    unit: '',
    status: ''
  });

  useEffect(() => {
    const resetFormData = { ...formData };
    if (!modalShow) {
      resetFormData.sku = '';
      resetFormData.product_name = '';
      resetFormData.qty = '';
      resetFormData.price = '';
      resetFormData.unit = '';
      resetFormData.status = '';
      setFormData(resetFormData);
    }
  }, []);

  const deleteItem = async item => {
    const token = localStorage.getItem('token');
    const header = {
      Authorization: 'Bearer',
      token
    };
    const items = { sku: item };
    await axios.post(endPoint.deleteProduct, items, header);
    try {
      await axios.post(endPoint.deleteProduct, items);
      history.push('/')
    } catch (error) {
      if (error) {
        console.log('DELETE FAILED', error);
      }
    }
  };

  const getDetail = async dataId => {
    let formGetDetail = {
      sku: dataId
    };
    try {
      const { data } = await axios.post(endPoint.getItem, formGetDetail);
      if (data) {
        let formData = {};
        formData['sku'] = data.sku;
        formData['product_name'] = data.product_name;
        formData['qty'] = data.qty;
        formData['price'] = data.price;
        formData['unit'] = data.unit;
        formData['status'] = data.status;
        setFormData(formData);
      }
    } catch (error) {
      if (error) {
        console.log('ERROR GET DETAIL DATA', error);
      }
    }
  };

  useEffect(() => {
    getListItems({ setListProduct });
  }, []);

  const search = async e => {
    if (e.key === 'Enter') {
      await getListItems({
        searchInputValue,
        setListProduct,
        setNotExist
      });
    }
  };

  const handleFieldChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const editData = dataId => {
    getDetail(dataId);
    setEditModalShow(true);
  };

  console.log(notExist, listProduct);
  if (!isLogin) {
    return (
      <AuthSplitLayout>
        <Row className="justify-content-center">
          <Col xs="auto">
            <Flex alignItems="center">
              <div>
                <h4>Hi!</h4>
                <p className="mb-0">
                  Don't have an account? <br />
                  <Link
                    className="light font-sans-serif z-index-1 position-relative"
                    to="/authentication/card/register"
                  >
                    Let's Get Started
                  </Link>
                </p>
              </div>
            </Flex>
          </Col>
        </Row>
      </AuthSplitLayout>
    );
  } else {
    return (
      <div>
        <Flex className="bg-white p-3 rounded mt-2 mb-3 justify-content-between">
          <Flex className="w-100 me-3">
            <Form.Control
              type="search"
              placeholder="Search..."
              aria-label="Search"
              className="rounded search-input"
              value={searchInputValue}
              onChange={({ target }) => setSearchInputValue(target.value)}
              onKeyPress={e => search(e)}
            />
          </Flex>
          <div className="d-flex justify-content-end w-100">
            <Button
              variant="primary"
              style={{ height: '50px' }}
              onClick={() => setModalShow(true)}
            >
              ADD PRODUCT
            </Button>
          </div>
        </Flex>
        <Table bordered responsive className="bg-white">
          <thead>
            <tr>
              <th scope="col">SKU</th>
              <th scope="col">Product Name</th>
              <th className="text-end" scope="col">
                Actions
              </th>
            </tr>
          </thead>
          {notExist || !listProduct ? (
            <tbody>
              <tr>
                <td>No Record exists!</td>
              </tr>
            </tbody>
          ) : (
            <tbody>
              {listProduct?.map((data, idx) => {
                return (
                  // eslint-disable-next-line react/jsx-key
                  <tr>
                    <td key={idx}>{data.sku}</td>
                    <td>{data.product_name}</td>
                    <td className="text-end">
                      <ActionButton
                        icon="edit"
                        title="Edit"
                        variant="action"
                        className="p-0 me-2"
                        onClick={() => editData(data.sku)}
                      />
                      <ActionButton
                        icon="trash-alt"
                        title="Delete"
                        variant="action"
                        className="p-0"
                        onClick={() => deleteItem({ item: data.sku })}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          )}
        </Table>
        <AddForm
          modalShow={modalShow}
          setModalShow={setModalShow}
          handleSubmit={e => handleSubmit({ e, formData, setModalShow })}
          formData={formData}
          handleFieldChange={handleFieldChange}
          setFormData={setFormData}
        />
        <EditForm
          editModalShow={editModalShow}
          setEditModalShow={setEditModalShow}
          handleSubmit={e => editProduct({ e, formData, setEditModalShow })}
          editFormData={formData}
          handleFieldChange={handleFieldChange}
          setEditFormData={setFormData}
        />
      </div>
    );
  }
};

export default Dashboard;
