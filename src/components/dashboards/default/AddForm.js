import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Modal, CloseButton } from 'react-bootstrap';
import Flex from 'components/common/Flex';

const AddForm = ({
  modalShow,
  setModalShow,
  handleSubmit,
  formData,
  handleFieldChange,
  setFormData
}) => {
  const resetForm = () => {
    const resetFormData = { ...formData };
    resetFormData.sku = '';
    resetFormData.product_name = '';
    resetFormData.qty = '';
    resetFormData.price = '';
    resetFormData.unit = '';
    resetFormData.status = '';
    setFormData(resetFormData);
    setModalShow(false);
  };
  return (
    <Modal
      show={modalShow}
      onHide={() => setModalShow(false)}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Form onSubmit={handleSubmit}>
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Add Product
          </Modal.Title>
          <CloseButton
            className="btn btn-circle btn-sm transition-base p-0"
            onClick={resetForm}
          />
        </Modal.Header>
        <Modal.Body className="px-5">
          <Form.Group className="mb-3">
            <Form.Label>SKU</Form.Label>
            <Form.Control
              placeholder="SKU"
              value={formData.sku}
              name="sku"
              onChange={handleFieldChange}
              type="text"
              style={{ height: '50px' }}
            />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              placeholder="Product Name"
              value={formData.Product_name}
              name="product_name"
              onChange={handleFieldChange}
              type="text"
              style={{ height: '50px' }}
            />
          </Form.Group>
          <Flex className="g-2 mb-3">
            <Form.Group className="me-3">
              <Form.Label>Qty</Form.Label>
              <Form.Control
                placeholder="Qty"
                value={formData.qty}
                name="qty"
                onChange={handleFieldChange}
                type="number"
                style={{ height: '50px' }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Price</Form.Label>
              <Form.Control
                placeholder="Price"
                value={formData.price}
                name="price"
                onChange={handleFieldChange}
                type="number"
                style={{ height: '50px' }}
              />
            </Form.Group>
          </Flex>
          <Flex className="g-2 mb-4">
            <Form.Group className="me-3">
              <Form.Label>Unit</Form.Label>
              <Form.Control
                placeholder="Unit"
                value={formData.unit}
                name="unit"
                onChange={handleFieldChange}
                type="text"
                style={{ height: '50px' }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Status</Form.Label>
              <Form.Control
                placeholder="Status"
                value={formData.status}
                name="status"
                onChange={handleFieldChange}
                type="number"
                style={{ height: '50px' }}
              />
            </Form.Group>
          </Flex>
        </Modal.Body>
        <Modal.Footer>
          <Button type="submit">SUBMIT</Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

AddForm.propTypes = {
  modalShow: PropTypes.bool
};

export default AddForm;
